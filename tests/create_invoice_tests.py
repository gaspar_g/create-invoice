import datetime
import unittest
from decimal import Decimal

from lib.check_account_number import CheckAccountNumber
from lib.collect_invoice_input_data import CollectInvoiceInputData
from lib.check_taxpayer_identification_number import CheckTaxpayerIdentificationNumber
from lib.exceptions import (
    InvalidAccountNumberLengthError,
    InvalidAccountNumberValueError,
    InvalidTINLengthError,
    InvalidTINValueError,
)

TEST_CONFIG_FILE = 'test_invoice_details.yaml'


class TestCollectInvoiceInputData(unittest.TestCase):

    def test_check_date_and_place(self):
        test = CollectInvoiceInputData(TEST_CONFIG_FILE)
        test._collect_payment_data()
        test._check_date_and_place()
        self.assertEqual(datetime.date.today().strftime('%d.%m.%Y'), test._payment['date'])
        self.assertEqual('Warszawa', test._payment['city'])

        test._payment['date'], test._payment['city'] = '01.01.2001', 'Kraków'
        test._check_date_and_place()
        self.assertEqual('01.01.2001', test._payment['date'])
        self.assertEqual('Kraków', test._payment['city'])

    def test_check_signature(self):
        test = CollectInvoiceInputData(TEST_CONFIG_FILE)
        test._collect_payment_data()

        self.assertFalse(test._check_signature())

        test._signature['with_signature'] = 'yes'
        self.assertTrue(test._check_signature())

    def test_collect_payment_data(self):
        test = CollectInvoiceInputData(TEST_CONFIG_FILE)
        test._collect_payment_data()
        self.assertEqual('100-08-2018', test._title)
        self.assertEqual(False, test._signature['with_signature'])
        self.assertEqual('Podpisany Imieniem i Nazwiskiem', test._signature['signature_text'])
        self.assertEqual('Przykładowa Firma', test._seller['name'])
        self.assertEqual('Przykładowa ulica', test._seller['street'])
        self.assertEqual('00-000 Przykładowe Miasto', test._seller['city'])
        self.assertEqual(3750645501, test._seller['taxpayer_id_number'])
        self.assertEqual(None, test._payment['date'])
        self.assertEqual(None, test._payment['city'])
        self.assertEqual('Bank', test._payment['bank_name'])
        self.assertEqual('PL 07 1090 2428 0774 8952 2047 0673', test._payment['bank_account'])
        self.assertEqual('Przelew', test._payment['type'])
        self.assertEqual('7 dni', test._payment['date_of_payment'])
        self.assertEqual('Przykładowe_imię Przykładowe_nazwisko', test._buyer['name'])
        self.assertEqual('ul. Przykładowa 1', test._buyer['street'])
        self.assertEqual('00-000 Przykładowe Europejskie Miasto', test._buyer['city'])
        self.assertEqual('37-506-455-01', test._buyer['taxpayer_id_number'], )
        self.assertEqual('Przykładowa usługa 1 - 23% VAT', test._services_data[0].description)
        self.assertEqual(Decimal('1'), test._services_data[0].quantity)
        self.assertEqual(Decimal('1000'), test._services_data[0].service_net)
        self.assertEqual(Decimal('23'), test._services_data[0].tax_percent)
        self.assertEqual('Przykładowa usługa 2 - 23% VAT', test._services_data[1].description)
        self.assertEqual(Decimal('2'), test._services_data[1].quantity)
        self.assertEqual(Decimal('2000'), test._services_data[1].service_net)
        self.assertEqual(Decimal('23'), test._services_data[1].tax_percent)


class TestCheckTaxpayerIdentificationNumber(unittest.TestCase):

    def test_check_tin(self):
        # Too long TIN
        test = CheckTaxpayerIdentificationNumber(37506455012)
        with self.assertRaises(InvalidTINLengthError):
            test.check_tin()

        # Invalid TIN value
        test = CheckTaxpayerIdentificationNumber(3750645506)
        with self.assertRaises(InvalidTINValueError):
            test.check_tin()

    def test_match_tin(self):
        test = CheckTaxpayerIdentificationNumber(3750645501)
        test._match_tin()
        self.assertEqual('3750645501', test._tin_digits)

        test = CheckTaxpayerIdentificationNumber('375@064@55@01')
        test._match_tin()
        self.assertEqual('3750645501', test._tin_digits)

        test = CheckTaxpayerIdentificationNumber('375-064-55-01')
        test._match_tin()
        self.assertEqual('3750645501', test._tin_digits)

        test = CheckTaxpayerIdentificationNumber('064-55-01')
        with self.assertRaises(InvalidTINLengthError):
            test._match_tin()

        test = CheckTaxpayerIdentificationNumber('064-55-01-12345')
        with self.assertRaises(InvalidTINLengthError):
            test._match_tin()


class TestCheckAccountNumber(unittest.TestCase):
    """
    Sample correct acc number used in tests: PL 07 1090 2428 0774 8952 2047 0672 taken from:
    http://www.genapps.pl/generatory/iban
    """

    def test_check_if_account_number_is_valid(self):
        too_long_acc_num = 'PL 07 1090 2428 0774 8952 2047 06734'  # Added one more digit
        test = CheckAccountNumber(too_long_acc_num)
        with self.assertRaises(InvalidAccountNumberLengthError):
            test.check_if_account_number_is_valid()

        wrong_country_code = 'DE 07 1090 2428 0774 8952 2047 0673'  # DE instead of PL
        test = CheckAccountNumber(wrong_country_code)
        with self.assertRaises(InvalidAccountNumberLengthError):
            test.check_if_account_number_is_valid()

        incorrect_acc_num = 'PL 07 1090 2428 0774 8952 2047 0672'  # Last digit 2 instead of 3
        test = CheckAccountNumber(incorrect_acc_num)
        with self.assertRaises(InvalidAccountNumberValueError):
            test.check_if_account_number_is_valid()

    def test_find_remainder(self):
        correctly_converted_acc_1 = '109024280774895220470673252107'
        wrongly_converted_acc = '1090242807748952204706732521073'
        find_remainder_test = CheckAccountNumber(correctly_converted_acc_1)
        self.assertTrue(find_remainder_test._find_remainder(correctly_converted_acc_1))
        self.assertFalse(find_remainder_test._find_remainder(wrongly_converted_acc))

    def test_convert_checksum_number(self):
        acc_num = 'PL 07 1090 2428 0774 8952 2047 0672'
        expected_converted_num = '252107109024280774895220470672'
        converter_test = CheckAccountNumber(acc_num)
        self.assertEqual(expected_converted_num, converter_test._convert_checksum_number(acc_num))

        acc_num = '07 1090 2428 0774 8952 2047 0672'
        converter_test = CheckAccountNumber(acc_num)
        self.assertEqual(expected_converted_num,  converter_test._convert_checksum_number(acc_num))

    def test_convert_account_number(self):
        test = CheckAccountNumber('test')
        acc_num = 'PL 07 1090 2428 0774 8952 2047 0672'
        expected_num = '109024280774895220470672252107'

        test._convert_account_number(acc_num)
        self.assertEqual(expected_num, test.converted_acc_number)

        acc_num = '07 1090 2428 0774 8952 2047 0672'
        test._convert_account_number(acc_num)
        self.assertEqual(expected_num, test.converted_acc_number)

        acc_num = '07109024280774895220470672'
        test._convert_account_number(acc_num)
        self.assertEqual(expected_num, test.converted_acc_number)

        acc_num = 'DE 27114020040000300201355387'
        with self.assertRaises(InvalidAccountNumberLengthError):
            test._convert_account_number(acc_num)


if __name__ == '__main__':
    unittest.main()
