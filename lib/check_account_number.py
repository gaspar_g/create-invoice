#!/usr/bin/python3

from lib.exceptions import (
    InvalidAccountNumberLengthError,
    InvalidAccountNumberValueError,
)

NUMBER_OF_CHARACTERS_IN_IBAN_NUMBER = 28
NUMBER_OF_DIGITS_IN_ACC_NUMBER = 26
POLAND_COUNTRY_CODE = 'PL'
PL_COUNTRY_CODE_REPLACEMENT = '2521'

REMAINDER = 97
MODULO_RESULT = 1


class CheckAccountNumber:
    def __init__(self, acc_number):
        self._acc_number = acc_number
        self.converted_acc_number = ''

    def check_if_account_number_is_valid(self):
        """
        Raises InvalidAccountNumberErrors if account number is not valid. To verify, we need to
        find checksum and to do so - we take first 4 signs if given number starts with PL or 2
        signs/digits if not. Then we need to convert 'PL' + 2 most left digits to 2521 XX where
        'PL' is replaced by 2521 and XX are these 2 digit. Add it to end of string-number.
        If new, converted number % 97 (REMAINDER) == 1 (MODULO_RESULT), acc number is valid.
        For now it works only for PL Banks.
        """
        self._convert_account_number(self._acc_number)
        if not self._find_remainder(self.converted_acc_number):
            raise InvalidAccountNumberValueError(self._acc_number)

    def _convert_account_number(self, number):
        temp_acc_number = "".join(str(number).split())
        if ((len(temp_acc_number) == NUMBER_OF_CHARACTERS_IN_IBAN_NUMBER)
                and temp_acc_number.startswith(POLAND_COUNTRY_CODE)):

            checksum_number = temp_acc_number[:4]
            converted_checksum_number = self._convert_checksum_number(checksum_number)
            self.converted_acc_number = temp_acc_number[4:] + converted_checksum_number

        elif ((len(temp_acc_number) == NUMBER_OF_DIGITS_IN_ACC_NUMBER)
                and not temp_acc_number.startswith(POLAND_COUNTRY_CODE)):
            checksum_number = temp_acc_number[:2]
            converted_checksum_number = self._convert_checksum_number(checksum_number)
            self.converted_acc_number = temp_acc_number[2:] + converted_checksum_number
        else:
            raise InvalidAccountNumberLengthError(number)

    @staticmethod
    def _convert_checksum_number(checksum_number):
        if 'PL' in checksum_number:
            checksum_num = "".join(
                checksum_number.replace('PL', PL_COUNTRY_CODE_REPLACEMENT).replace(' ', ''))
            return checksum_num
        else:
            return "".join((PL_COUNTRY_CODE_REPLACEMENT + checksum_number).replace(' ', ''))

    @staticmethod
    def _find_remainder(converted_acc_number):
        return int(converted_acc_number) % REMAINDER == MODULO_RESULT
