#!/usr/bin/python3

import re

from lib.exceptions import (
    InvalidTINLengthError,
    InvalidTINValueError,
)


# TIN as Taxpayer Identification Number
TIN_REGEX = re.compile('\d+')

# Multipliers needed to check validity of TIN
MULTIPLIERS = [6, 5, 7, 2, 3, 4, 5, 6, 7]

# Const. TIN modulo needed to do final calc
TIN_MODULO = 11

CORRECT_TIN_LENGTH = 10


class CheckTaxpayerIdentificationNumber:
    """
    Raises InvalidTIN Errors when TIN [pl. NIP] is invalid.
    TIN like 00-000-000-00 etc. is valid but here used as sample TIN. In real life such TIN is
    pointless and shouldn't be used.
    """

    def __init__(self, taxpayer_id_number):
        self._taxpayer_id_number = str(taxpayer_id_number)
        self._tin_digits = ''

    def check_tin(self):
        self._match_tin()
        self._make_calculations()

    def _match_tin(self):
        self._tin_digits = "".join(TIN_REGEX.findall(self._taxpayer_id_number))
        if not len(self._tin_digits) == CORRECT_TIN_LENGTH:
            raise InvalidTINLengthError(self._taxpayer_id_number)

    def _make_calculations(self):
        temp_results = []
        for tin_digit, multiplier_digit in zip(self._tin_digits[:9], MULTIPLIERS):
            temp_results.append(int(tin_digit) * int(multiplier_digit))
        if not sum(temp_results) % TIN_MODULO == int(self._tin_digits[9]):
            raise InvalidTINValueError(self._taxpayer_id_number)
