#!/usr/bin/python3

import datetime
import os
from decimal import Decimal
from typing import NamedTuple

import formencode_jinja2
import jinja2
import pdfkit
from slownie import slownie_zl

from lib.collect_invoice_input_data import CollectInvoiceInputData

CONFIG_FILE = "invoice_details.yaml"
INVOICES_PATH = f"Faktury/{datetime.date.today().strftime('%m_%Y')}"
BASE_TEMPLATE_PATH = "template_files/"
BASE_TEMPLATE_FILE_NAME = "base_invoice_template.html"
DEFAULT_DATE = datetime.date.today().strftime('%d.%m.%Y')
DEFAULT_CITY = 'Warszawa'


class CollectedServiceValues(NamedTuple):
    tax_percent: Decimal
    net_amount: Decimal
    tax_amount: Decimal
    gross_amount: Decimal


def prepare_directory():
    if not os.path.exists(INVOICES_PATH):
        os.makedirs(INVOICES_PATH)


class CreateInvoice:

    def __init__(self):
        self.title = dict()
        self.signature = bool
        self.seller = dict()
        self.buyer = dict()
        self.payment = dict()

        self.service_tax_and_net_values = dict()
        self.collected_service_values = list()
        self.services_data = list()

        self.total_gross = int
        self.amount_in_words = str
        self._html_output = str

    def create_invoice(self):
        self._get_data()
        self._calculate_total_gross()
        self._write_total_gross_amount_in_words()
        self._render_template()
        self._write_invoice_to_files()

    def _get_data(self):
        invoice_data = CollectInvoiceInputData(CONFIG_FILE)
        invoice_data.collect_and_check_data()
        (self.title, self.seller, self.signature,
         self.buyer, self.payment, self.services_data) = invoice_data.data

    def _calculate_total_gross(self):
        self._calculate_net_values()
        self._calculate_gross_values()
        self.total_gross = sum(service.gross_amount for service in self.collected_service_values)

    def _calculate_net_values(self):
        for service in self.services_data:
            if service.tax_percent in self.service_tax_and_net_values:
                self.service_tax_and_net_values[service.tax_percent] += service.service_net
            else:
                self.service_tax_and_net_values[service.tax_percent] = service.service_net

    def _calculate_gross_values(self):
        for tax_percent, net_amount in self.service_tax_and_net_values.items():
            tax = net_amount * (tax_percent / 100)
            total = net_amount + tax
            self.collected_service_values.append(CollectedServiceValues(tax_percent=tax_percent,
                                                                        net_amount=net_amount,
                                                                        tax_amount=tax,
                                                                        gross_amount=total))

    def _write_total_gross_amount_in_words(self):
        self.amount_in_words = slownie_zl(self.total_gross)

    def _render_template(self):
        template_loader = jinja2.FileSystemLoader(searchpath=BASE_TEMPLATE_PATH)
        template_env = jinja2.Environment(loader=template_loader)
        template_env.add_extension(formencode_jinja2.formfill)

        template = template_env.get_template(BASE_TEMPLATE_FILE_NAME)
        self._html_output = template.render(long_template_version=self._check_template_version(),
                                            signature=self.signature,
                                            title=self.title,
                                            seller=self.seller,
                                            buyer=self.buyer,
                                            services=self.services_data,
                                            payment=self.payment,
                                            calculated_services=self.collected_service_values,
                                            total_gross=self.total_gross,
                                            amount_in_words=self.amount_in_words,
                                            )

    def _check_template_version(self):
        # long version : len > 1
        return len(self.services_data) > 1

    def _write_invoice_to_files(self):
        invoice = f"{INVOICES_PATH}/Faktura {self.title}"
        if os.path.exists(f"{invoice}.html") and os.path.exists(f"{invoice}.pdf"):
            text = 'Faktura o podanym numerze już istnieje. Czy chcesz ją zastąpić? t/n \n'
            user_response = input(text)
            if user_response == 't' or user_response == 'tak':
                self._create_html_file()
                self._create_pdf_file()
            else:
                print('Zmień numer faktury i spróbuj ponownie.')
                return
        else:
            self._create_html_file()
            self._create_pdf_file()

    def _create_html_file(self):
        invoice = f"{INVOICES_PATH}/Faktura {self.title}.html"
        with open(invoice, 'w') as file:
            file.write(self._html_output)

    def _create_pdf_file(self):
        html_file_path = f'{INVOICES_PATH}/Faktura {self.title}.html'
        pdf_file_path = f'{INVOICES_PATH}/Faktura {self.title}.pdf'
        pdfkit.from_file(html_file_path, pdf_file_path, options={'quiet': ''})


def create_invoice():
    prepare_directory()
    invoice = CreateInvoice()
    invoice.create_invoice()


if __name__ == '__main__':
    create_invoice()
