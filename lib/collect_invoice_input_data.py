#!/usr/bin/python3

import datetime
import re
import yaml
from collections import namedtuple
from decimal import Decimal, DecimalException
from typing import NamedTuple

from lib.check_account_number import CheckAccountNumber
from lib.check_taxpayer_identification_number import CheckTaxpayerIdentificationNumber
from lib.exceptions import (
    InvalidAmountError,
    InvalidConfigFileFormatError,
    InvalidTaxPercentError,
    InvalidQuantityError,
)

DEFAULT_DATE = datetime.date.today().strftime('%d.%m.%Y')
DEFAULT_CITY = 'Warszawa'
INVOICE_DATA = namedtuple(
    'Invoice_details', ['title', 'seller', 'signature', 'buyer', 'payment', 'services_data'])

QUANTITY_REGEX = re.compile('(\d+[,.]\d+|\d+)')


class ServiceData(NamedTuple):
    ordinal_number: int
    description: str
    quantity: Decimal
    price_net: Decimal
    service_net: Decimal
    tax_percent: Decimal


class CollectedServiceValues(NamedTuple):
    tax_percent: Decimal
    net_amount: Decimal
    tax_amount: Decimal
    gross_amount: Decimal


class CollectInvoiceInputData:
    def __init__(self, config_file):
        self.config_file = config_file
        self.data = namedtuple
        self._title = dict()
        self._seller = dict()
        self._signature = dict()
        self._buyer = dict()
        self._payment = dict()
        self._description = str
        self._amount = Decimal
        self._tax_percent = Decimal
        self._quantity = Decimal
        self._service_net = Decimal
        self._services_data = list()

    def collect_and_check_data(self):
        self._collect_payment_data()
        self._check_signature()
        self._check_taxpayer_identification_numbers()
        self._check_and_convert_payment_and_signature_data()

        self.data = INVOICE_DATA(
            self._title, self._seller, self._signature, self._buyer, self._payment,
            self._services_data)

    def _collect_payment_data(self):
        with open(self.config_file, 'r') as data:
            try:
                config = yaml.load(data)
            except yaml.YAMLError:
                raise InvalidConfigFileFormatError(self.config_file)
            self._title = config['title']['number']
            self._signature = config['signature']
            self._seller = config['seller']
            self._buyer = config['buyer']
            self._payment = config['payment']
            for ord_num, service in enumerate(config['services'].values(), start=1):
                # there could be a check for anything within single service (TEST_CONFIG_FILE),
                # cause all boxes have to be filled, so I just picked the first one.
                if service['description']:
                    self._description = service['description']

                    self._amount, self._quantity, self._tax_percent = (
                                                    self._check_and_convert_service_data(service))
                    self._service_net = self._amount * self._quantity
                    self._services_data.append(ServiceData(ordinal_number=ord_num,
                                                           description=self._description,
                                                           quantity=self._quantity,
                                                           price_net=self._amount,
                                                           service_net=self._service_net,
                                                           tax_percent=self._tax_percent,
                                                           ))

    def _check_and_convert_payment_and_signature_data(self):
        self._check_date_and_place()
        self._check_config_account_number_data(self._payment['bank_account'])

    def _check_and_convert_service_data(self, service):
        amount = self._check_service_amount_data(service['amount'])
        quantity = self._check_service_quantity_data(service['quantity'])
        tax_percent = self._check_service_tax_percent_data(service['tax_percent'])
        return amount, quantity, tax_percent

    def _check_signature(self):
        return self._signature['with_signature'] == 'yes'

    def _check_date_and_place(self):
        if not self._payment['date']:
            self._payment['date'] = DEFAULT_DATE
        if not self._payment['city']:
            self._payment['city'] = DEFAULT_CITY

    def _check_taxpayer_identification_numbers(self):
        seller_tin = CheckTaxpayerIdentificationNumber(self._seller['taxpayer_id_number'])
        seller_tin.check_tin()
        buyer_tin = CheckTaxpayerIdentificationNumber(self._buyer['taxpayer_id_number'])
        buyer_tin.check_tin()

    @staticmethod
    def _check_config_account_number_data(account_number):
        check_acc = CheckAccountNumber(account_number)
        check_acc.check_if_account_number_is_valid()

    @staticmethod
    def _check_service_tax_percent_data(tax_percent_data):
        try:
            return Decimal(tax_percent_data)
        except DecimalException:
            raise InvalidTaxPercentError(tax_percent_data)

    @staticmethod
    def _check_service_quantity_data(quantity_data):
        try:
            if type(quantity_data) is str:
                return Decimal(QUANTITY_REGEX.match(quantity_data).group())
            else:
                return Decimal(quantity_data)
        except AttributeError:
            raise InvalidQuantityError(quantity_data)

    @staticmethod
    def _check_service_amount_data(amount_data):
        try:
            if type(amount_data) is str:
                return Decimal(amount_data.replace(',', '.'))
            else:
                return Decimal(amount_data)
        except DecimalException:
            raise InvalidAmountError(amount_data)
