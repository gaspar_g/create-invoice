#!/usr/bin/python3


class InvoiceError(Exception):
    pass


class InvalidAccountNumberError(InvoiceError):
    """
    Raises InvalidAccountNumberInvoiceException when there is invalid account number
    """
    def __init__(self, value=None, msg=None):
        self.value = value
        self.msg = msg

    def __str__(self):
        return f'\n\n{self.value} - {self.msg}'


class InvalidAccountNumberValueError(InvalidAccountNumberError):
    """Raises when there is invalid checksum"""
    def __init__(self, value):
        self.value = value
        self.msg = 'Nieprawidłowy numer rachunku.'


class InvalidAccountNumberLengthError(InvalidAccountNumberError):
    """Raises when there is invalid number of characters in account number"""
    def __init__(self, value):
        self.value = value
        self.msg = 'Podany numer rachunku bankowego posiada nieprawidłową liczbę znaków (cyfr).'


class InvalidAmountError(InvoiceError):
    """Raises when there is invalid input amount """
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return f'\n\n{self.value} - Nieprawidłowa kwota za usługę.'


class InvalidTaxPercentError(InvoiceError):
    """Raises when there is invalid input tax percent"""
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return f'\n\n{self.value} - Wprowadzony podatek za usługę nie jest prawidłowy.'


class InvalidQuantityError(InvoiceError):
    """
    Raises InvalidQuantityInvoiceException when there is something wrong with quantity
    (i.e. there is no int/float)
    """
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return f'\n\n{self.value} - Nieprawidłowa liczba/ilość usług.'


class InvalidTaxpayerIdentificationNumberError(InvoiceError):
    """
    Raises Invalid Taxpayer Identification [as TIN pl. NIP] Number when TIN is invalid
    (i.e. TIN has 9 numbers, instead of 10).
    """
    def __init__(self, value=None, msg=None):
        self.value = value
        self.msg = msg

    def __str__(self):
        return f'\n\n{self.value} - {self.msg}'


class InvalidTINValueError(InvalidTaxpayerIdentificationNumberError):
    """Raises when there is invalid modulo == incorrect TIN"""
    def __init__(self, value=None):
        self.value = value
        self.msg = 'Nieprawidłowy NIP.'

    def __str__(self):
        return f'\n\n{self.value} - {self.msg}'


class InvalidTINLengthError(InvalidTaxpayerIdentificationNumberError):
    """Raises when there is invalid number of characters in TIN"""
    def __init__(self, value):
        self.value = value
        self.msg = 'Podany NIP zawiera nieprawidłową liczbę znaków (cyfr).'

    def __str__(self):
        return f'\n\n{self.value} - {self.msg}'


class InvalidConfigFileFormatError(InvoiceError):
    """
    Raises InvalidConfigFileFormatError exception when there is something wrong with .yaml config
    file (i.e. quantity:1 instead of quantity: 1).
    """
    def __init__(self, config_file_name):
        self.config_file_name = config_file_name
        self.msg = f'Błąd/nieprawidłowe formatowanie w pliku {self.config_file_name}'

    def __str__(self):
        return f'\n\n{self.msg}'
