#!/usr/bin/python3

from lib.invoice_generator import create_invoice


def main():
    create_invoice()


if __name__ == '__main__':
    main()
